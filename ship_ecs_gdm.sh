#!/usr/bin/env bash

#-----------------------------------------------------------------------------#
### Script Overview
# This script is built to deploy using default parameters from the 
# gobii-web.properties file 

#-----------------------------------------------------------------------------#
### Running the Script
# Running the script and what is needed to run with proper variables and 
# Parameters

# !!! Always try to run scripts with the output is logged for tracability and 
# !!! trouble shooting.

#>>> Example: TAG=$(date +"%Y-%m-%d_%H-%M-%S") && bash ship_ecs_gdm.sh gobii-web.parameters 2>&1 |& tee -a deployment_logs/ship_ecs_gdm_$TAG.log


#-----------------------------------------------------------------------------#
### Authors(s)
#@author: (rpetrie) rlp243@cornell.edu

#-----------------------------------------------------------------------------#
### Global 
#-----------------------------------------------------------------------------#

# Treat unset variables and parameters other than the special parameters ‘@’ 
# or ‘*’ as an error when performing parameter expansion. An error message will
# be written to the standard error, and a non-interactive shell will exit.
set -u

set -e # Exit on non-zero status
# set -x # Heavy verbosity

#-----------------------------------------------------------------------------#
### Sourcing, Variables & Parameters
#-----------------------------------------------------------------------------#

# source gobii-web.properties

#-----------------------------------------------------------------------------#
# 

ecs-cli compose --file gobii-web.yml --ecs-params ecs-params.yml start --launch-type EC2 --create-log-groups

#-----------------------------------------------------------------------------#
# 

echo;
echo "Script Completion. Exiting Script... Good Bye!"